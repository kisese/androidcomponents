package com.material.kisese

import android.app.Application
import android.support.v7.app.AppCompatDelegate

class Material : Application() {
    companion object {
        lateinit var instance: Material
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }
}